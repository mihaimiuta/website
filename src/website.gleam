import argv
import assets
import config
import constants
import gleam/io
import gleam/option.{type Option, None, Some}
import gleam/result
import lustre/ssg
import tailwind
import types

fn get_variant() -> Option(types.WebsiteVariant) {
  case argv.load().arguments {
    ["mihaimiuta"] -> Some(types.MihaiMiuta)
    ["ivaldea"] -> Some(types.Ivaldea)
    _ -> None
  }
}

pub fn main() {
  let assert Some(variant) = get_variant()
  use config <- result.try(variant |> config.get())

  let build_result =
    config.build_config
    |> ssg.build()
    |> result.map(fn(_) {
      [
        "--config=" <> config.paths.tailwind_config,
        // TODO: Create separate css inputs
        "--input=./src/css/app.css",
        "--output=" <> constants.output_folder <> "/css/app.css",
      ]
      |> tailwind.install_and_run()
    })

  case build_result {
    Ok(_) -> {
      let _ = assets.copy(config.paths.assets)
      io.println("Build succeeded: " <> config.url)

      Ok(Nil)
    }
    Error(error) -> {
      io.debug(error)
      io.println("Build failed: " <> config.url)

      Error(Nil)
    }
  }
}
