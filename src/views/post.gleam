import gleam/list
import gleam/result
import gleam/string
import htmgrrrl.{Characters, StartElement}
import lustre/attribute.{class}
import lustre/element.{element, text}
import lustre/element/html
import views/layout.{layout}

fn extract_info(state, _line, event) {
  case event {
    Characters(text) -> [text, ..state]
    StartElement(_uri, local_name, _qualified_name, _attributes) -> {
      [local_name, ..state]
    }
    _ -> state
  }
}

// TODO: Fix these for nested html
fn map_lines(lines) {
  Ok(
    lines
    |> list.filter(fn(line) {
      line != "body" && line != "head" && line != "html"
    })
    |> list.sized_chunk(into: 2)
    |> list.map(fn(chunk) {
      let assert [element_content, element_type] = chunk

      element(element_type, [], [text(element_content)])
    })
    |> list.reverse(),
  )
}

fn convert(content: String) {
  let content = "<div>" <> content <> "</div>"
  let content = string.replace(content, each: "\n", with: "")

  use lines <- result.try(content |> htmgrrrl.sax([], extract_info))

  map_lines(lines)
}

pub fn view(content: String) {
  let children = result.unwrap(convert(content), [])

  layout([
    html.div(
      [class("flex flex-col justify-center items-center gap-10")],
      children,
    ),
  ])
}
