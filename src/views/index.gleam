import lustre/attribute.{attribute}
import lustre/element.{text}
import lustre/element/html
import views/layout.{layout}

pub fn view() {
  layout([
    html.h1([attribute.class("text-4xl")], [text("Hi, I'm Mihai")]),
    html.h2([], [text("I like to talk about code, history and philosophy")]),
  ])
}
