import lustre/attribute.{attribute}
import lustre/element.{type Element}
import lustre/element/html

pub fn layout(body: List(Element(a))) {
  html.html([attribute("lang", "en")], [
    html.head([], [
      html.link([
        attribute.href("/apple-touch-icon.png"),
        attribute("sizes", "180x180"),
        attribute.rel("apple-touch-icon"),
      ]),
      html.link([
        attribute.href("/favicon-32x32.png"),
        attribute("sizes", "32x32"),
        attribute.type_("image/png"),
        attribute.rel("icon"),
      ]),
      html.link([
        attribute.href("/favicon-16x16.png"),
        attribute("sizes", "16x16"),
        attribute.type_("image/png"),
        attribute.rel("icon"),
      ]),
      html.link([attribute.href("/site.webmanifest"), attribute.rel("manifest")]),
      html.link([attribute.href("/css/app.css"), attribute.rel("stylesheet")]),
      html.link([
        attribute.href("https://fonts.googleapis.com"),
        attribute.rel("preconnect"),
      ]),
      html.link([
        attribute("crossorigin", ""),
        attribute.href("https://fonts.gstatic.com"),
        attribute.rel("preconnect"),
      ]),
      html.title([], "Mihai Miuta"),
      html.link([
        attribute.rel("stylesheet"),
        attribute.href(
          "https://fonts.googleapis.com/css2?family=Merriweather+Sans&display=swap",
        ),
      ]),
      html.meta([attribute("charset", "UTF-8")]),
      html.meta([
        attribute("content", "Just a guy really. I like philosophy tho."),
        attribute.name("description"),
      ]),
      html.meta([
        attribute(
          "content",
          "follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large",
        ),
        attribute.name("robots"),
      ]),
      html.meta([
        attribute("content", "width=device-width, initial-scale=1"),
        attribute.name("viewport"),
      ]),
      html.meta([
        attribute(
          "content",
          "mihaimiuta.com/xgo git https://github.com/miutamihai/xgo",
        ),
        attribute.name("go-import"),
      ]),
      html.link([
        attribute.href("https://mihaimiuta.com"),
        attribute.rel("canonical"),
      ]),
      html.meta([
        attribute("content", "en_US"),
        attribute("property", "og:locale"),
      ]),
      html.meta([
        attribute("content", "website"),
        attribute("property", "og:type"),
      ]),
      html.meta([
        attribute("content", "Mihai Miuta"),
        attribute("property", "og:title"),
      ]),
      html.meta([
        attribute("content", "Just a guy really. I like philosophy tho."),
        attribute("property", "og:description"),
      ]),
      html.meta([
        attribute("content", "https://mihaimiuta.com"),
        attribute("property", "og:url"),
      ]),
      html.meta([
        attribute("content", "Mihai Miuta"),
        attribute("property", "og:site_name"),
      ]),
      html.meta([
        attribute("content", "summary"),
        attribute.name("twitter:card"),
      ]),
      html.meta([
        attribute("content", "Mihai Miuta"),
        attribute.name("twitter:title"),
      ]),
      html.meta([
        attribute("content", "Just a guy really. I like philosophy tho."),
        attribute.name("twitter:description"),
      ]),
    ]),
    html.body(
      [
        attribute.class(
          "min-h-screen font-sans text-white bg-gradient-to-br from-primary to-secondary backdrop-blur-md",
        ),
      ],
      [
        html.div(
          [
            attribute.class(
              "min-h-screen max-w-prose flex flex-col items-center justify-center m-auto py-12",
            ),
          ],
          body,
        ),
      ],
    ),
  ])
}
