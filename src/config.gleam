import constants
import gleam/result
import lustre/ssg
import posts
import types
import views/index
import views/post

pub fn get(variant: types.WebsiteVariant) -> Result(types.Website, Nil) {
  let build = ssg.new(constants.output_folder)

  case variant {
    types.MihaiMiuta -> {
      use post_map <- result.try(posts.get())
      let config =
        build
        |> ssg.add_static_route("/", index.view())
        |> ssg.add_dynamic_route("/blog", post_map, post.view)

      Ok(types.Website(
        variant,
        url: "https://mihaimiuta.com",
        paths: types.Paths(
          assets: "assets",
          posts: "posts",
          tailwind_config: "tailwind/mihaimiuta.config.js",
        ),
        build_config: config,
      ))
    }
    types.Ivaldea -> {
      // TODO Use appropriate values here
      get(types.MihaiMiuta)
    }
  }
}
