import lustre/ssg.{
  type Config, type HasStaticRoutes, type NoStaticDir, type UseDirectRoutes,
}

pub type WebsiteVariant {
  MihaiMiuta
  Ivaldea
}

pub type Paths {
  Paths(assets: String, posts: String, tailwind_config: String)
}

pub type Website {
  Website(
    variant: WebsiteVariant,
    url: String,
    paths: Paths,
    build_config: Config(HasStaticRoutes, NoStaticDir, UseDirectRoutes),
  )
}
