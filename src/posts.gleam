import constants
import gleam/dict
import gleam/list
import gleam/result
import gleam/string
import simplifile

@external(erlang, "Elixir.MDEx", "to_html")
fn to_html(input: String) -> String

fn get_file_names() {
  constants.posts_folder |> simplifile.get_files() |> result.nil_error()
}

fn route_from_file_name(file_name: String) {
  file_name
  |> string.split(on: "/")
  |> list.last()
  |> result.nil_error()
  |> result.map(fn(file_name) {
    string.split(file_name, on: ".") |> list.first()
  })
}

fn read_files(file_names: List(String)) {
  file_names
  |> list.map(fn(file_name) {
    use actual_filename <- result.try(file_name |> route_from_file_name())

    simplifile.read(file_name)
    |> result.nil_error()
    |> result.map(fn(content) { #(actual_filename, content) })
  })
  |> result.all()
}

fn convert(tuple: #(Result(String, Nil), String)) {
  let #(file_name, content) = tuple

  #(file_name |> result.unwrap(""), content |> to_html())
}

pub fn get() -> Result(dict.Dict(String, String), Nil) {
  use file_names <- result.try(get_file_names())

  file_names
  |> read_files()
  |> result.map(fn(files) { files |> list.map(convert) })
  |> result.map(dict.from_list)
}
