import constants
import gleam/list
import gleam/result
import gleam/string
import simplifile.{get_files}

pub fn copy(assets_path: String) {
  use files <- result.try(get_files(assets_path) |> result.nil_error())

  list.map(files, fn(file) {
    use file_name <- result.try(string.split(file, on: "/") |> list.last())
    let destination = constants.output_folder <> "/" <> file_name

    use _ <- result.try(
      simplifile.copy_file(file, destination) |> result.nil_error(),
    )

    Ok(Nil)
  })
  |> result.all()
}
