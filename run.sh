#!/usr/bin/env sh

set -e

gleam run $1 && npx serve output
